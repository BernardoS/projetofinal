O projeto final da disciplina será composto de uma aplicação CRUD
conténdo [backend](https://gitlab.com/BernardoS/projetointermediario) desenvolvido em NodeJS e frontend desenvolvido usando
o framework ReactJS.

## Requisitos
1. O frontend deverá ser desenvolvido utilizando o framework ReactJS
2. O backend deverá ser composto de uma API, usando express e NodeJS,
com pelo menos 5 recursos diferentes contendo as seguintes operações:
- Consulta Geral ( GET )
- Consulta Única ( GET )
- Inserção ( POST )
- Atualização ( PUT )
- Deleção ( DELETE )
3. Deverá ser estilizado usando CSS e, preferencialmente, com um
framework como Bootstrap, Foundation ou Material Design.
4. O design do frontend deverá ser responsivo e adaptar-se à dispositivos
móveis
5. Os erros retornados ao realizar as operações na API, como erro ao
inserir, deverão ser tratados e retornados ao usuário.
6. O acesso do frontend à API deve ser feito utilizando uma chave de
acesso (autenticação).
7. Para cada um dos 5 recursos, o usuário deve ser capaz de visualizar
todos os itens e um item em particular.
8. Para cada um dos 5 recursos, o usuário deve ser capaz de inserir e
deletar um item.
9. Para cada um dos 5 recursos, o usuário deve ser capaz de editar um
item.
10. Para cada um dos 5 recursos, o usuário deve ser capaz de pesquisar por
um ou mais itens usando um filtro de pesquisa.

