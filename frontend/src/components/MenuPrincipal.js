import React from 'react'
import CharacterPage from '../components/Character/CharacterPage'
import EnemyPage from '../components/Enemies/EnemyPage'
import ItemPage from '../components/Items/ItemPage'
import MapPage from '../components/Maps/MapPage'
import PlacePage from '../components/Places/PlacePage'
import UserPage from '../components/Users/UserPage'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import logo from '../icons/rpg.png'

class MenuPrincipal extends React.Component {

    constructor(props) {
        super(props)

        this.state = { pagina: null }
    }

    trocaPagina = (pag) => {
        switch (pag) {
            case 1: {
                this.setState({ pagina: <CharacterPage /> })
                break
            }
            case 2: {
                this.setState({ pagina: <EnemyPage /> })
                break
            }
            case 3: {
                this.setState({ pagina: <ItemPage /> })
                break
            }
            case 4: {
                this.setState({ pagina: <MapPage /> })
                break
            }
            case 5: {
                this.setState({ pagina: <PlacePage /> })
                break
            }
            case 6: {
                this.setState({ pagina: <UserPage /> })
                break
            }
            default: {
                this.setState({ pagina: null })
                break
            }
        }
    }

    render() {

        return (

            <div>

                <Navbar bg="dark" variant="dark" expand="sm">
                    <Navbar.Brand><img
                        src={logo}
                        className="d-inline-block align-top"
                        alt="Projeto Final Logo"
                    /></Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                            <Nav.Link onClick={this.trocaPagina.bind(this, 1)} >Characters</Nav.Link>
                            <Nav.Link onClick={this.trocaPagina.bind(this, 2)} href="">Enemies</Nav.Link>
                            <Nav.Link onClick={this.trocaPagina.bind(this, 3)} href="">Items</Nav.Link>
                            <Nav.Link onClick={this.trocaPagina.bind(this, 4)} href="">Maps</Nav.Link>
                            <Nav.Link onClick={this.trocaPagina.bind(this, 5)} href="">Places</Nav.Link>
                            <Nav.Link onClick={this.trocaPagina.bind(this, 6)} href="">Users</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>

                {this.state.pagina}

            </div>
        )
    }
}

export default MenuPrincipal