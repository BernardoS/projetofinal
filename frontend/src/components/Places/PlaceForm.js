import React from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Col from 'react-bootstrap/Col'
import Badge from 'react-bootstrap/Badge'

class PlaceForm extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            name: this.props.name,
            description: this.props.description,
            map: this.props.map,
            alerta: ""

        }
    }

    handleChange = (event) => {
        const inputName = event.target.name
        const inputValue = event.target.value
        this.setState({ [inputName]: inputValue })

    }

    alerta = () => {
        if (this.state.name === undefined || this.state.name === "") {
            this.setState({ alerta: "Name é um campo obrigatório!" })
        } else {
            this.setState({ alerta: "" })
        }
    }

    handleClick = () => {
        this.alerta()
        var data = {
            "name": this.state.name, "description": this.state.description,
            "map": this.state.map
        }
        this.props.handleAction(data)
    }

    render() {

        return (

            <div>

                <Form onSubmit={this.handleClick}>
                    <Form.Row className="justify-content-md-center">
                        <Col sm="3">
                            <Form.Label >Name</Form.Label>
                            <Form.Control name="name" value={this.state.name} onChange={this.handleChange} />
                        </Col>
                        <Col sm="3">
                            <Form.Label>Description</Form.Label>
                            <Form.Control name="description" value={this.state.description} onChange={this.handleChange} />
                        </Col>
                    </Form.Row>
                    <Form.Row className="justify-content-md-center">
                        <Col sm="3">
                            <Form.Label>Map(id)</Form.Label>
                            <Form.Control value={this.state.map} name="map" onChange={this.handleChange} />
                        </Col>
                    </Form.Row>

                    <Form.Row className="justify-content-md-end">
                        <Button varient="primary" onClick={this.handleClick} >Confirmar</Button>

                    </Form.Row>

                </Form>

                <Badge position="static" variant="danger">{this.state.alerta}</Badge>

                <br></br>

            </div >
        )

    }
}

export default PlaceForm