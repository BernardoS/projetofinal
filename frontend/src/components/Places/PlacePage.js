import React from 'react'
import axios from 'axios'
import PlaceItem from './PlaceItem'
import PlaceForm from './PlaceForm'
import Container from 'react-bootstrap/Container'
import ListGroup from 'react-bootstrap/ListGroup'
import Badge from 'react-bootstrap/Badge'

class PlacePage extends React.Component {

    constructor(props) {
        super(props)
        this.BASE_URL = "http://localhost:4000/api/places"
        this.state = { places: [], selectedPlace: undefined }
        this.loadPlace()
    }

    loadPlace = () => {
        axios.get(this.BASE_URL).then((response) => {
            this.setState({ places: response.data })
        }).catch((error) => {
            console.error(error)
        })
    }

    handleAction = (data) => {
        if (this.state.selectedPlace) {
            var id = this.state.selectedPlace._id
            this.handleUpdate(id, data)
        } else {
            this.handleInsert(data)
        }
    }

    selectPlace = (places) => {
        if (this.state.selectedPlace) {
            places = this.state.selectedPlace._id === places._id ? undefined : places
        }

        this.setState({ selectedPlace: places })
    }

    handleUpdate = (id, data) => {
        axios.put(this.BASE_URL + "/" + id, data).then(() => {
            this.loadPlace()
        }).catch((error) => {
            console.error(error)
        })
    }

    handleInsert = (data) => {
        axios.post(this.BASE_URL, data).then(() => {
            this.loadPlace()
        }).catch((error) => {
            console.error(error)
        })
    }

    handleDelete = (id) => {
        axios.delete(this.BASE_URL + "/" + id).then(() => {
            this.loadPlace()
        })
        this.setState({ selectedMap: undefined })
    }

    render() {
        if (this.state.selectedPlace) {
            var editLabel = <div>Editando: {this.state.selectedPlace.name}</div>
        } else {
            editLabel = "Inserindo"
        }

        var PlaceList = this.state.places.map((value) => {
            return <PlaceItem
                key={value._id}
                value={value}
                handleDelete={(id) => this.handleDelete(id)}
                handleSelect={(id) => this.selectPlace(id)}
            />
        })

        let { name, description, map, _id } = this.state.selectedPlace ? this.state.selectedPlace : ""

        var placeForm = <PlaceForm
            handleAction={(data) => this.handleAction(data)}
            key={_id}
            name={name}
            description={description}
            map={map}
        />

        return <div>

            <Badge position="static" variant="warning">{editLabel}</Badge>

            <Container> {placeForm}</Container>

            <ListGroup variant="flush">
                <ListGroup.Item>{PlaceList}</ListGroup.Item>

            </ListGroup>

        </div>
    }
}

export default PlacePage