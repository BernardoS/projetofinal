import React from 'react'
import Badge from 'react-bootstrap/Badge'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Col from 'react-bootstrap/Col'
import imgDelete from '../../icons/delete.png'

class MapItem extends React.Component {

    handleDelete = () => {
        this.props.handleDelete(this.props.value._id)
    }

    handleSelect = () => {
        this.props.handleSelect(this.props.value)
    }

    render() {

        return (
            <div>

                <Form.Row>

                    <Col sm="11">

                        <Button variant="light" block onClick={this.handleSelect}>
                            {this.props.value.name} <Badge variant="dark">id: {this.props.value._id}</Badge>
                        </Button>

                    </Col>
                    <Col sm="1">
                        <Button block onClick={this.handleDelete} variant="danger">
                            <img
                                src={imgDelete}
                                className="d-inline-block align-top"
                                alt="Deletar"
                            />
                        </Button>

                    </Col>
                </Form.Row>

            </div >
        )
    }
}

export default MapItem