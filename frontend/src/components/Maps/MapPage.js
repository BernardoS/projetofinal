import React from 'react'
import axios from 'axios'
import MapItem from './MapItem'
import MapForm from './MapForm'
import Container from 'react-bootstrap/Container'
import ListGroup from 'react-bootstrap/ListGroup'
import Badge from 'react-bootstrap/Badge'

class MapPage extends React.Component {

    constructor(props) {
        super(props)
        this.BASE_URL = "http://localhost:4000/api/maps"
        this.state = { maps: [], selectedMap: undefined }
        this.loadMap()
    }

    loadMap = () => {
        axios.get(this.BASE_URL).then((response) => {
            this.setState({ maps: response.data })
        }).catch((error) => {
            console.error(error)
        })
    }

    handleAction = (data) => {
        if (this.state.selectedMap) {
            var id = this.state.selectedMap._id
            this.handleUpdate(id, data)
        } else {
            this.handleInsert(data)
        }
    }

    selectMap = (maps) => {
        if (this.state.selectedMap) {
            maps = this.state.selectedMap._id === maps._id ? undefined : maps
        }

        this.setState({ selectedMap: maps })
    }

    handleUpdate = (id, data) => {
        axios.put(this.BASE_URL + "/" + id, data).then(() => {
            this.loadMap()
        }).catch((error) => {
            console.error(error)
        })
    }

    handleInsert = (data) => {
        axios.post(this.BASE_URL, data).then(() => {
            this.loadMap()
        }).catch((error) => {
            console.error(error)
        })
    }

    handleDelete = (id) => {
        axios.delete(this.BASE_URL + "/" + id).then(() => {
            this.loadMap()
        })
        this.setState({ selectedMap: undefined })
    }

    render() {
        if (this.state.selectedMap) {
            var editLabel = <div>Editando: {this.state.selectedMap.name}</div>
        } else {
            editLabel = "Inserindo"
        }

        var MapList = this.state.maps.map((value) => {
            return <MapItem
                key={value._id}
                value={value}
                handleDelete={(id) => this.handleDelete(id)}
                handleSelect={(id) => this.selectMap(id)}
            />
        })

        let { name, description, type, _id } = this.state.selectedMap ? this.state.selectedMap : ""

        var mapForm = <MapForm
            handleAction={(data) => this.handleAction(data)}
            key={_id}
            name={name}
            description={description}
            type={type}
        />

        return <div>

            <Badge position="static" variant="warning">{editLabel}</Badge>

            <Container> {mapForm}</Container>

            <ListGroup variant="flush">
                <ListGroup.Item>{MapList}</ListGroup.Item>

            </ListGroup>

        </div>
    }
}

export default MapPage