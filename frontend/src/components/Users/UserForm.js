import React from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Col from 'react-bootstrap/Col'
import Badge from 'react-bootstrap/Badge'

class UserForm extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            name: this.props.name,
            password: this.props.password,
            admin: this.props.admin,
            alerta: ""

        }
    }

    handleChange = (event) => {
        const inputName = event.target.name
        const inputValue = event.target.value
        this.setState({ [inputName]: inputValue })

    }

    alerta = () => {
        if (this.state.name === undefined || this.state.name === "" || this.state.password === undefined || this.state.password === "") {
            this.setState({ alerta: "Name e Password são campos obrigatórios" })
        } else {
            this.setState({ alerta: "" })
        }
    }

    handleClick = () => {
        this.alerta()
        var data = {
            "name": this.state.name, "password": this.state.password,
            "admin": this.state.admin
        }
        this.props.handleAction(data)
    }

    render() {

        return (

            <div>

                <Form onSubmit={this.handleClick}>
                    <Form.Row className="justify-content-md-center">
                        <Col sm="3">
                            <Form.Label >Name</Form.Label>
                            <Form.Control name="name" value={this.state.name} onChange={this.handleChange} />
                        </Col>
                        <Col sm="3">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" name="password" value={this.state.password} onChange={this.handleChange} />
                        </Col>
                        <Col sm="3">
                            <Form.Label>Admin</Form.Label>
                            <Form.Control value={this.state.admin} name="admin" onChange={this.handleChange} placeholder="false" />
                        </Col>

                    </Form.Row>

                    <Form.Row className="justify-content-md-end">
                        <Button varient="primary" onClick={this.handleClick} >Confirmar</Button>

                    </Form.Row>

                </Form>

                <Badge position="static" variant="danger">{this.state.alerta}</Badge>

                <br></br>

            </div >
        )

    }
}

export default UserForm