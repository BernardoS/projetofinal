import React from 'react'
import axios from 'axios'
import UserItem from './UserItem'
import UserForm from './UserForm'
import Container from 'react-bootstrap/Container'
import ListGroup from 'react-bootstrap/ListGroup'
import Badge from 'react-bootstrap/Badge'

class UserPage extends React.Component {

    constructor(props) {
        super(props)
        this.BASE_URL = "http://localhost:4000/api/users"
        this.state = { users: [], selectedUser: undefined }
        this.loadUser()
    }

    loadUser = () => {
        axios.get(this.BASE_URL).then((response) => {
            this.setState({ users: response.data })
        }).catch((error) => {
            console.error(error)
        })
    }

    handleAction = (data) => {
        if (this.state.selectedUser) {
            var id = this.state.selectedUser._id
            this.handleUpdate(id, data)
        } else {
            this.handleInsert(data)
        }
    }

    selectUser = (users) => {
        if (this.state.selectedUser) {
            users = this.state.selectedUser._id === users._id ? undefined : users
        }

        this.setState({ selectedUser: users })
    }

    handleUpdate = (id, data) => {
        axios.put(this.BASE_URL + "/" + id, data).then(() => {
            this.loadUser()
        }).catch((error) => {
            console.error(error)
        })
    }

    handleInsert = (data) => {
        axios.post(this.BASE_URL, data).then(() => {
            this.loadUser()
        }).catch((error) => {
            console.error(error)
        })
    }

    handleDelete = (id) => {
        axios.delete(this.BASE_URL + "/" + id).then(() => {
            this.loadUser()
        })
        this.setState({ selectedUser: undefined })
    }

    render() {
        if (this.state.selectedUser) {
            var editLabel = <div>Editando: {this.state.selectedUser.name}</div>
        } else {
            editLabel = "Inserindo"
        }

        var UserList = this.state.users.map((value) => {
            return <UserItem
                key={value._id}
                value={value}
                handleDelete={(id) => this.handleDelete(id)}
                handleSelect={(id) => this.selectUser(id)}
            />
        })

        let { name, password, admin, _id } = this.state.selectedUser ? this.state.selectedUser : ""

        var userForm = <UserForm
            handleAction={(data) => this.handleAction(data)}
            key={_id}
            name={name}
            password={password}
            admin={admin}
        />

        return <div>

            <Badge position="static" variant="warning">{editLabel}</Badge>

            <Container> {userForm}</Container>

            <ListGroup variant="flush">
                <ListGroup.Item>{UserList}</ListGroup.Item>

            </ListGroup>

        </div>
    }
}

export default UserPage