import React from 'react'
import axios from 'axios'
import ItemItem from './ItemItem'
import ItemForm from './ItemForm'
import Container from 'react-bootstrap/Container'
import ListGroup from 'react-bootstrap/ListGroup'
import Badge from 'react-bootstrap/Badge'

class ItemPage extends React.Component {

    constructor(props) {
        super(props)
        this.BASE_URL = "http://localhost:4000/api/items"
        this.state = { items: [], selectedItem: undefined }
        this.loadItem()
    }

    loadItem = () => {
        axios.get(this.BASE_URL).then((response) => {
            this.setState({ items: response.data })
        }).catch((error) => {
            console.error(error)
        })
    }

    handleAction = (data) => {
        if (this.state.selectedItem) {
            var id = this.state.selectedItem._id
            this.handleUpdate(id, data)
        } else {
            this.handleInsert(data)
        }
    }

    selectItem = (items) => {
        if (this.state.selectedItem) {
            items = this.state.selectedItem._id === items._id ? undefined : items
        }

        this.setState({ selectedItem: items })
    }

    handleUpdate = (id, data) => {
        axios.put(this.BASE_URL + "/" + id, data).then(() => {
            this.loadItem()
        }).catch((error) => {
            console.error(error)
        })
    }

    handleInsert = (data) => {
        axios.post(this.BASE_URL, data).then(() => {
            this.loadItem()
        }).catch((error) => {
            console.error(error)
        })
    }

    handleDelete = (id) => {
        axios.delete(this.BASE_URL + "/" + id).then(() => {
            this.loadItem()
        })
        this.setState({ selectedItem: undefined })
    }

    render() {
        if (this.state.selectedItem) {
            var editLabel = <div>Editando: {this.state.selectedItem.name}</div>
        } else {
            editLabel = "Inserindo"
        }

        var ItemList = this.state.items.map((value) => {
            return <ItemItem
                key={value._id}
                value={value}
                handleDelete={(id) => this.handleDelete(id)}
                handleSelect={(id) => this.selectItem(id)}
            />
        })

        let { name, description, effect, owner, _id } = this.state.selectedItem ? this.state.selectedItem : ""

        var itemForm = <ItemForm
            handleAction={(data) => this.handleAction(data)}
            key={_id}
            name={name}
            description={description}
            effect={effect}
            owner={owner}
        />

        return <div>

            <Badge position="static" variant="warning">{editLabel}</Badge>

            <Container> {itemForm}</Container>

            <ListGroup variant="flush">
                <ListGroup.Item>{ItemList}</ListGroup.Item>

            </ListGroup>

        </div>
    }
}

export default ItemPage