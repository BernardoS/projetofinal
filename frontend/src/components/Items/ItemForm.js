import React from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Col from 'react-bootstrap/Col'
import Badge from 'react-bootstrap/Badge'

class ItemForm extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            name: this.props.name,
            description: this.props.description,
            effect: this.props.effect,
            owner: this.props.owner,
            alerta: ""

        }

    }

    alerta = () => {
        if (this.state.name === undefined || this.state.name === ""
            || this.state.effect === undefined || this.state.effect === "") {
            this.setState({ alerta: "Name e Effect são campos obrigatórios" })
        } else {
            this.setState({ alerta: "" })
        }
    }

    handleChange = (event) => {
        const inputName = event.target.name
        const inputValue = event.target.value
        this.setState({ [inputName]: inputValue })

    }

    handleClick = () => {
        this.alerta()
        var data = {
            "name": this.state.name, "description": this.state.description,
            "effect": this.state.effect, "owner": this.state.owner
        }
        this.props.handleAction(data)
    }

    render() {

        return (

            <div>

                <Form onSubmit={this.handleClick}>
                    <Form.Row className="justify-content-md-center">
                        <Col sm="3">
                            <Form.Label >Name</Form.Label>
                            <Form.Control name="name" value={this.state.name} onChange={this.handleChange} />
                        </Col>
                        <Col sm="3">
                            <Form.Label>Description</Form.Label>
                            <Form.Control name="description" value={this.state.description} onChange={this.handleChange} />
                        </Col>
                    </Form.Row>
                    <Form.Row className="justify-content-md-center">
                        <Col sm="3">
                            <Form.Label>Effect</Form.Label>
                            <Form.Control value={this.state.effect} name="effect" onChange={this.handleChange} />
                        </Col>
                        <Col sm="3">
                            <Form.Label>Character(id)</Form.Label>
                            <Form.Control value={this.state.owner} name="owner" onChange={this.handleChange} />
                        </Col>
                    </Form.Row>

                    <Form.Row className="justify-content-md-end">
                        <Button varient="primary" onClick={this.handleClick} >Confirmar</Button>

                    </Form.Row>

                </Form>

                <Badge position="static" variant="danger">{this.state.alerta}</Badge>

                <br></br>

            </div >
        )

    }
}

export default ItemForm