import React from 'react'
import axios from 'axios'
import CharacterItem from './CharacterItem'
import CharacterForm from './CharacterForm'
import Container from 'react-bootstrap/Container'
import ListGroup from 'react-bootstrap/ListGroup'
import Badge from 'react-bootstrap/Badge'

class CharacterPage extends React.Component {

    constructor(props) {
        super(props)
        this.BASE_URL = "http://localhost:4000/api/characters"
        this.state = { characters: [], selectedCharacter: undefined, alerta: ""}
        this.loadCharacters()
    }

    loadCharacters = () => {
        axios.get(this.BASE_URL).then((response) => {
            this.setState({ characters: response.data })
        }).catch((error) => {
            console.error(error)
        })
    }

    handleAction = (data) => {
        if (this.state.selectedCharacter) {
            var id = this.state.selectedCharacter._id
            this.handleUpdate(id, data)
        } else {
            this.handleInsert(data)
        }
    }

    selectCharacter = (character) => {
        if (this.state.selectedCharacter) {
            character = this.state.selectedCharacter._id === character._id ? undefined : character
        }

        this.setState({ selectedCharacter: character })
    }

    handleUpdate = (id, data) => {
        axios.put(this.BASE_URL + "/" + id, data).then(() => {
            this.loadCharacters()
        }).catch((error) => {
            console.error(error)
        })
    }

    handleInsert = (data) => {
        axios.post(this.BASE_URL, data).then(() => {
            this.loadCharacters()
        }).catch((error) => {
            console.error(error)
        })
    }

    handleDelete = (id) => {
        axios.delete(this.BASE_URL + "/" + id).then(() => {
            this.loadCharacters()
        })
        this.setState({ selectedCharacter: undefined })
    }

    render() {
        if (this.state.selectedCharacter) {
            var editLabel = <div>Editando: {this.state.selectedCharacter.name}</div>
        } else {
            editLabel = "Inserindo"
        }

        var characterList = this.state.characters.map((value) => {
            return <CharacterItem
                key={value._id}
                value={value}
                handleDelete={(id) => this.handleDelete(id)}
                handleSelect={(id) => this.selectCharacter(id)}
            />
        })

        let { name, classe, level, exp, health, armor, gold, _id } = this.state.selectedCharacter ? this.state.selectedCharacter : ""

        var characterForm = <CharacterForm
            handleAction={(data) => this.handleAction(data)}
            key={_id}
            name={name}
            class={classe}
            level={level}
            exp={exp}
            health={health}
            armor={armor}
            gold={gold}

        />
        

        return <div>

            <Badge position="static" variant="warning">{editLabel}</Badge>
           

            <Container> {characterForm}</Container>

            <ListGroup variant="flush">
                <ListGroup.Item>{characterList}</ListGroup.Item>

            </ListGroup>


        </div>
    }
}

export default CharacterPage