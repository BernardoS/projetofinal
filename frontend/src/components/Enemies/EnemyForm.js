import React from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Col from 'react-bootstrap/Col'
import Badge from 'react-bootstrap/Badge'

class EnemyForm extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            name: this.props.name,
            class: this.props.class,
            description: this.props.description,
            level: this.props.level,
            health: this.props.health,
            armor: this.props.armor,
            alerta: ""

        }
    }

    handleChange = (event) => {
        const inputName = event.target.name
        const inputValue = event.target.value
        this.setState({ [inputName]: inputValue })

    }

    alerta = () => {
        if (this.state.name === undefined || this.state.name === ""
            || this.state.class === undefined || this.state.class === "") {
            this.setState({ alerta: "Name e Class são campos obrigatórios!" })
        } else {
            this.setState({ alerta: "" })
        }
    }

    handleClick = () => {
        this.alerta()
        var data = {
            "name": this.state.name, "classe": this.state.class, "description": this.state.description,
            "level": this.state.level, "health": this.state.health,
            "armor": this.state.armor
        }
        this.props.handleAction(data)
    }

    render() {

        return (

            <div>

                <Form onSubmit={this.handleClick}>
                    <Form.Row className="justify-content-md-center">
                        <Col sm="3">
                            <Form.Label >Name</Form.Label>
                            <Form.Control name="name" value={this.state.name} onChange={this.handleChange} />
                        </Col>
                        <Col sm="3">
                            <Form.Label>Class</Form.Label>
                            <Form.Control name="class" value={this.state.class} onChange={this.handleChange} />
                        </Col>
                        <Col sm="3">
                            <Form.Label>Description</Form.Label>
                            <Form.Control value={this.state.description} name="description" onChange={this.handleChange} />
                        </Col>
                    </Form.Row >
                    <Form.Row className="justify-content-md-center">
                        <Col sm="2">
                            <Form.Label>Health</Form.Label>
                            <Form.Control value={this.state.health} name="health" type="number" min="100" max="999" onChange={this.handleChange} />
                        </Col>
                        <Col sm="2">
                            <Form.Label>Armor</Form.Label>
                            <Form.Control value={this.state.armor} name="armor" type="number" min="100" max="9999" onChange={this.handleChange} />
                        </Col>
                        <Col sm="1">
                            <Form.Label>Level</Form.Label>
                            <Form.Control value={this.state.level} name="level" type="number" min="1" max="99" onChange={this.handleChange} />
                        </Col>
                    </Form.Row>
                    <Form.Row className="justify-content-md-end">
                        <Button varient="primary" onClick={this.handleClick} >Confirmar</Button>


                    </Form.Row>

                </Form>

                <Badge position="static" variant="danger">{this.state.alerta}</Badge>

                <br></br>

            </div >
        )

    }
}

export default EnemyForm