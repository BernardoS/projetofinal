import React from 'react'
import axios from 'axios'
import EnemyItem from './EnemyItem'
import EnemyForm from './EnemyForm'
import Container from 'react-bootstrap/Container'
import ListGroup from 'react-bootstrap/ListGroup'
import Badge from 'react-bootstrap/Badge'

class EnemyPage extends React.Component {

    constructor(props) {
        super(props)
        this.BASE_URL = "http://localhost:4000/api/enemies"
        this.state = { enemies: [], selectedEnemy: undefined }
        this.loadEnemy()
    }

    loadEnemy = () => {
        axios.get(this.BASE_URL).then((response) => {
            this.setState({ enemies: response.data })
        }).catch((error) => {
            console.error(error)
        })
    }

    handleAction = (data) => {
        if (this.state.selectedEnemy) {
            var id = this.state.selectedEnemy._id
            this.handleUpdate(id, data)
        } else {
            this.handleInsert(data)
        }
    }

    selectEnemy = (enemies) => {
        if (this.state.selectedEnemy) {
            enemies = this.state.selectedEnemy._id === enemies._id ? undefined : enemies
        }

        this.setState({ selectedEnemy: enemies })
    }

    handleUpdate = (id, data) => {
        axios.put(this.BASE_URL + "/" + id, data).then(() => {
            this.loadEnemy()
        }).catch((error) => {
            console.error(error)
        })
    }

    handleInsert = (data) => {
        axios.post(this.BASE_URL, data).then(() => {
            this.loadEnemy()
        }).catch((error) => {
            console.error(error)
        })
    }

    handleDelete = (id) => {
        axios.delete(this.BASE_URL + "/" + id).then(() => {
            this.loadEnemy()
        })
        this.setState({ selectedEnemy: undefined })
    }

    render() {
        if (this.state.selectedEnemy) {
            var editLabel = <div>Editando: {this.state.selectedEnemy.name}</div>
        } else {
            editLabel = "Inserindo"
        }

        var EnemyList = this.state.enemies.map((value) => {
            return <EnemyItem
                key={value._id}
                value={value}
                handleDelete={(id) => this.handleDelete(id)}
                handleSelect={(id) => this.selectEnemy(id)}
            />
        })

        let { name, classe, description, level, health, armor, _id } = this.state.selectedEnemy ? this.state.selectedEnemy : ""

        var enemyForm = <EnemyForm
            handleAction={(data) => this.handleAction(data)}
            key={_id}
            name={name}
            class={classe}
            description={description}
            level={level}
            health={health}
            armor={armor}
        />

        return <div>

            <Badge position="static" variant="warning">{editLabel}</Badge>

            <Container> {enemyForm}</Container>

            <ListGroup variant="flush">
                <ListGroup.Item>{EnemyList}</ListGroup.Item>

            </ListGroup>


        </div>
    }
}

export default EnemyPage