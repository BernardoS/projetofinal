import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import MenuPrincipal from './components/MenuPrincipal';

function App() {
  return (
    <div>
      <MenuPrincipal/>
    </div>
  );
}

export default App;
