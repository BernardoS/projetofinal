const mongoDB = require('../data/connection')
var Schema = mongoDB.Schema
var mapSchema = new Schema({
    name: { type: String, required: true },
    description: { type: String },
    type: { type: String, required: true },

});

var map = mongoDB.model('Map', mapSchema)

module.exports = map