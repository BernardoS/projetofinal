const mongoDB = require('../data/connection')
var Schema = mongoDB.Schema

var enemiesSchema = new Schema({
    name: { type: String, required: true },
    classe: { type: String, required: true },
    description: { type: String },
    level: { type: Number, required: true, default: 1 },
    health: { type: Number, required: true, default: 100 },
    armor: { type: Number, required: true, default: 100 },
    attributes: {
        strength: { type: Number, required: true, default: 2 },
        constitution: { type: Number, required: true, default: 2 },
        speed: { type: Number, required: true, default: 2 },
        intelligence: { type: Number, required: true, default: 2 },
    },
});

var enemy = mongoDB.model('Enemy', enemiesSchema)

module.exports = enemy