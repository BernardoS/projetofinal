const mongoDB = require('../data/connection')
var Schema = mongoDB.Schema

var characterSchema = new Schema({
  name: { type: String, required: true },
  classe: { type: String, required: true },
  level: { type: Number, required: true, default: 1 },
  exp: { type: Number, required: true, default: 0 },
  health: { type: Number, required: true, default: 100 },
  armor: { type: Number, required: true, default: 100 },
  gold: { type: Number, required: true, default: 10 },
  attributes: {
    strength: { type: Number, required: true, default: 2 },
    constitution: { type: Number, required: true, default: 2 },
    speed: { type: Number, required: true, default: 2 },
    intelligence: { type: Number, required: true, default: 2 },
  },
});

var character = mongoDB.model('Character', characterSchema)

module.exports = character