const users_model = require('../models/users')
const mongoDB = require('./connection')

var query = { admin: true, name: 'Bernardo', password: 'adm123' }

users_model.findOne(query, function (err, doc) {
    if (doc != null && doc != []) {
        console.log("Usuário autenticado!")
        console.log(doc)

    } else {
        var db = mongoDB.connection;
        db.close()
        console.log("Usuário não autenticado, conexão com o banco fechada!")
    }
})