const express = require('express')
const router = express.Router()
const maps_model = require('../models/maps')

router.get("/", function (req, res) {
    var limit = Number(req.query.limit)
    var query = req.query.type ? {type: req.query.type} : {}
    maps_model.find(query).limit(limit).then(function (doc) {
        res.status(200).json(doc)
    }).catch(function () {
        res.status(400).json({ error: "An error occured while querying maps." })
    })
})

router.get('/:id', function (req, res) {
    var id = req.params.id
    maps_model.findById(id, function (err, doc) {
        if (err) {
            res.status(400).json({ error: "Map with id: " + id + " doesn't exist." })
        }
        res.status(200).json(doc)
    })
})

router.post('/', function (req, res) {

    var newMap = new maps_model(req.body)

    newMap.save(function (err, doc) {
        if (err) {
            res.status(400).json({ error: "An error occurred while adding a map." })
        }
        res.status(200).json(doc)
    })
})

router.put('/:id', function (req, res) {

    var id = req.params.id
    var mapUpdate = req.body
    maps_model.findByIdAndUpdate(id, mapUpdate, function (err, doc) {
        if (err) {
            res.status(400).json({ error: "An error occurred while updating a map." })
        }
        res.status(200).json(doc)
    })

})

router.delete('/:id', function (req, res) {

    var id = req.params.id
    maps_model.findByIdAndDelete(id, function (err, doc) {
        if (err) {
            res.status(400).json({ error: "An error occurred while deleting a map." })
        }
        res.status(200).json(doc)
    })
})

module.exports = router