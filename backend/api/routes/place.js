const express = require('express')
const router = express.Router()
const places_model = require('../models/places')

router.get("/", function (req, res) {
    var limit = Number(req.query.limit)
    var query = req.query.map ? {map: req.query.map} : {}
    places_model.find(query).limit(limit).then(function (doc) {
        res.status(200).json(doc)
    }).catch(function () {
        res.status(400).json({ error: "An error occured while querying places." })
    })
})

router.get('/:id', function (req, res) {
    var id = req.params.id
    places_model.findById(id, function (err, doc) {
        if (err) {
            res.status(400).json({ error: "Place with id: " + id + " doesn't exist." })
        }
        res.status(200).json(doc)
    })
})

router.post('/', function (req, res) {

    var newPlace = new places_model(req.body)

    newPlace.save(function (err, doc) {
        if (err) {
            res.status(400).json({ error: "An error occurred while adding a place." })
        }
        res.status(200).json(doc)
    })
})

router.put('/:id', function (req, res) {

    var id = req.params.id
    var placeUodate = req.body
    places_model.findByIdAndUpdate(id, placeUodate, function (err, doc) {
        if (err) {
            res.status(400).json({ error: "An error occurred while updating a place." })
        }
        res.status(200).json(doc)
    })

})

router.delete('/:id', function (req, res) {

    var id = req.params.id
    places_model.findByIdAndDelete(id, function (err, doc) {
        if (err) {
            res.status(400).json({ error: "An error occurred while deleting a place." })
        }
        res.status(200).json(doc)
    })
})

module.exports = router