const express = require('express')
const router = express.Router()
const characters_model = require('../models/characters')



router.get("/", function (req, res) {
    var limit = Number(req.query.limit)
    var query = req.query.class ? {class: req.query.class} : {}
    characters_model.find(query).limit(limit).then(function (doc) {
        res.status(200).json(doc)
    }).catch(function () {
        res.status(400).json({ error: "An error occurred while querying characters." })
    })
})

router.get('/:id', function (req, res) {
    var id = req.params.id
    characters_model.findById(id, function (err, doc) {
        if (err) {
            res.status(400).json({ error: "Character with id: " + id + " doesn't exist." })
        }
        res.status(200).json(doc)
    })
})

router.post('/', function (req, res) {

    var newCharacter = new characters_model(req.body)

    newCharacter.save(function (err, doc) {
        if (err) {
            res.status(400).json({ error: "An error occurred while adding a character." })
        }
        res.status(200).json(doc)
    })
})

router.put('/:id', function (req, res) {

    var id = req.params.id
    var characterUpdate = req.body
    characters_model.findByIdAndUpdate(id, characterUpdate, function (err, doc) {
        if (err) {
            res.status(400).json({ error: "An error occurred while updating a character." })
        }
        res.status(200).json(doc)
    })

})

router.delete('/:id', function (req, res) {

    var id = req.params.id
    characters_model.findByIdAndDelete(id, function (err, doc) {
        if (err) {
            res.status(400).json({ error: "An error occurred while deleting a character." })
        }
        res.status(200).json(doc)
    })
})

module.exports = router