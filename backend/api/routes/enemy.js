const express = require('express')
const router = express.Router()
const enemies_model = require('../models/enemies')

router.get("/", function (req, res) {
    var limit = Number(req.query.limit)
    var query = req.query.level ? {level: req.query.level} : {}
    enemies_model.find(query).limit(limit).then(function (doc) {
        res.status(200).json(doc)
    }).catch(function () {
        res.status(400).json({ error: "An error occurred while querying enemies." })
    })
})


router.get('/:id', function (req, res) {
    var id = req.params.id
    enemies_model.findById(id, function (err, doc) {
        if (err) {
            res.status(400).json({ error: "Enemy with id: " + id + " doesn't exist." })
        }
        res.status(200).json(doc)
    })
})

router.post('/', function (req, res) {

    var newEnemy = new enemies_model(req.body)

    newEnemy.save(function (err, doc) {
        if (err) {
            res.status(400).json({ error: "An error occurred while adding an enemy." })
        }
        res.status(200).json(doc)
    })
})

router.put('/:id', function (req, res) {

    var id = req.params.id
    var enemyUpdate = req.body
    enemies_model.findByIdAndUpdate(id, enemyUpdate, function (err, doc) {
        if (err) {
            res.status(400).json({ error: "An error occurred while updating an enemy." })
        }
        res.status(200).json(doc)
    })

})

router.delete('/:id', function (req, res) {

    var id = req.params.id
    enemies_model.findByIdAndDelete(id, function (err, doc) {
        if (err) {
            res.status(400).json({ error: "An error occurred while deleting an enemy." })
        }
        res.status(200).json(doc)
    })
})

module.exports = router