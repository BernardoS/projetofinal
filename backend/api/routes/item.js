const express = require('express')
const router = express.Router()
const items_model = require('../models/items')


router.get("/", function (req, res) {
    var limit = Number(req.query.limit)
    var query = req.query.effect ? {effect: req.query.effect} : {}
    items_model.find(query).limit(limit).then(function (doc) {
        res.status(200).json(doc)
    }).catch(function () {
        res.status(400).json({ error: "An error occurred while querying items" })
    })
})

router.get('/:id', function (req, res) {
    var id = req.params.id
    items_model.findById(id, function (err, doc) {
        if (err) {
            res.status(400).json({ error: "Item with id: " + id + " doesn't exist." })
        }
        res.status(200).json(doc)
    })
})

router.post('/', function (req, res) {

    var newItem = new items_model(req.body)

    newItem.save(function (err, doc) {
        if (err) {
            res.status(400).json({ error: "An error occurred while adding an item." })
        }
        res.status(200).json(doc)
    })
})

router.put('/:id', function (req, res) {

    var id = req.params.id
    var itemUpdate = req.body
    items_model.findByIdAndUpdate(id, itemUpdate, function (err, doc) {
        if (err) {
            res.status(400).json({ error: "An error occurred while updating an item." })
        }
        res.status(200).json(doc)
    })

})

router.delete('/:id', function (req, res) {

    var id = req.params.id
    items_model.findByIdAndDelete(id, function (err, doc) {
        if (err) {
            res.status(400).json({ error: "An error occurred while deleting an item." })
        }
        res.status(200).json(doc)
    })
})

module.exports = router