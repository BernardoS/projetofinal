const express = require('express')
const router = express.Router()
const users_model = require('../models/users')

router.get("/", function (req, res) {
    var limit = Number(req.query.limit)
    var query = req.query.admin ? {admin: req.query.admin} : {}
    users_model.find(query).limit(limit).then(function (doc) {
        res.status(200).json(doc)
    }).catch(function () {
        res.status(400).json({ error: "An error occured while querying users." })
    })
})

router.get('/:id', function (req, res) {
    var id = req.params.id
    users_model.findById(id, function (err, doc) {
        if (err) {
            res.status(400).json({ error: "User with id: " + id + " doesn't exist." })
        }
        res.status(200).json(doc)
    })
})

router.post('/', function (req, res) {

    var newUser = new users_model(req.body)

    newUser.save(function (err, doc) {
        if (err) {
            res.status(400).json({ error: "An error occurred while adding a user" })
        }
        res.status(200).json(doc)
    })
})

router.put('/:id', function (req, res) {

    var id = req.params.id
    var userUpdate = req.body
    users_model.findByIdAndUpdate(id, userUpdate, function (err, doc) {
        if (err) {
            res.status(400).json({ error: "An error occurred while updating a user" })
        }
        res.status(200).json(doc)
    })

})

router.delete('/:id', function (req, res) {

    var id = req.params.id
    users_model.findByIdAndDelete(id, function (err, doc) {
        if (err) {
            res.status(400).json({ error: "An error occurred while deleting a user" })
        }
        res.status(200).json(doc)
    })
})

module.exports = router