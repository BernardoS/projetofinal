// Configurações do Express
const express = require('express')
const app = express()
const port = 4000
const cors = require('cors')

app.use(cors("Access-Control-Allow-Origin", "*"))
app.use(express.json())

// Autenticação de usuário
require('./api/data/authentication')

// Rotas
const users_router = require('./api/routes/user')
const characters_router = require('./api/routes/character')
const enemies_router = require('./api/routes/enemy')
const items_router = require('./api/routes/item')
const maps_router = require('./api/routes/map')
const places_router = require('./api/routes/place')

app.use('/api/users', users_router)
app.use('/api/characters', characters_router)
app.use('/api/enemies', enemies_router)
app.use('/api/items', items_router)
app.use('/api/maps', maps_router)
app.use('/api/places', places_router)

app.listen(port, () => console.log(`Rodando...`))